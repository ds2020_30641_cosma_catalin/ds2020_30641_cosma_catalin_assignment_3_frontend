import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { CaregiversService } from "../caregivers.service";
import { Caregiver } from '../caregiver';
import { PatientsService } from "../patients.service";
import { Patient } from '../patient';
import { tap } from "rxjs/operators";

@Component({
  selector: 'app-caregiver-administration',
  templateUrl: './caregiver-administration.component.html',
  styleUrls: ['./caregiver-administration.component.css']
})
export class CaregiverAdministrationComponent implements OnInit {


  caregivers = [];
  patients = [];
  associatedPatients = [];
  names = [];

  caregiverId = new FormControl("");
  caregiverName = new FormControl("");
  caregiverPassword = new FormControl("");
  caregiverBirthDate = new FormControl("");
  caregiverGender = new FormControl("");
  caregiverAddress = new FormControl("");

  foundCaregiver: Caregiver;
  insertedCaregiver: Caregiver;
  updatedCaregiver: Caregiver;
  selectedCaregiver: Caregiver;
  deletedCaregiver: Caregiver;

  foundPatient: Patient;
  selectedPatient: Patient;

  patientsToAdd = [];
  patientsToRemove = [];
  patientsUpdated = [];
  patientsAvailable = [];

  constructor(private caregiversService: CaregiversService, private patientsService: PatientsService) { }

  ngOnInit(): void {
    this.caregiversService.getCaregivers().subscribe(data => (this.caregivers = data));
    this.patientsService.getPatients().subscribe(data => (this.patients = data));
  }

  RowSelected(u: any) {
    this.selectedCaregiver = u;
    this.getCaregiverById(this.selectedCaregiver.id).subscribe(
      () => (
        this.caregiverName.reset(""),
        this.caregiverPassword.reset(""),
        this.caregiverGender.reset(""),
        this.caregiverBirthDate.reset(""),
        this.caregiverAddress.reset(""),
        this.caregiverName.setValue(this.foundCaregiver.name),
        this.caregiverPassword.setValue(this.foundCaregiver.password),
        this.caregiverGender.setValue(this.foundCaregiver.gender),
        this.caregiverBirthDate.setValue(this.foundCaregiver.birthDate),
        this.caregiverAddress.setValue(this.foundCaregiver.address),
        this.associatedPatients = this.foundCaregiver.patients,
        this.patientsUpdated = this.foundCaregiver.patients
      )
    );
  }

  addPatientToAssociatedList() {
    for (let i of this.associatedPatients) {
      this.names.push(i.name);
    }

    if (this.names.includes(this.foundPatient.name) == false) {
      this.associatedPatients.push(this.foundPatient);
    }

    this.names = [];

  }



  patientRowSelected(u: any) {
    this.selectedPatient = u;
    this.getPatientById(this.selectedPatient.id).subscribe(
      () => (
        this.addPatientToAssociatedList()
      )
    );
  }

  associatedRowSelected(u: any) {
    this.selectedPatient = u;
    this.getPatientById(this.selectedPatient.id).subscribe(
      () => (this.associatedPatients.splice(this.associatedPatients.indexOf(this.selectedPatient), 1)
      )
    );
  }

  getAllCaregivers() {
    this.caregiversService
      .getCaregivers()
      .subscribe(data => (this.caregivers = data));
  }

  getAllPatients() {
    this.patientsService
      .getPatients()
      .subscribe(data => (this.patients = data));
  }

  getCaregiverById(id: string) {
    return this.caregiversService
      .getCaregiverById(id)
      .pipe(tap(data => (this.foundCaregiver = data)));
  }

  getPatientById(id: string) {
    return this.patientsService
      .getPatientById(id)
      .pipe(tap(data => (this.foundPatient = data)));
  }

  insertNewCaregiver() {
    this.insertedCaregiver = {
      id: this.caregiverId.value,
      name: this.caregiverName.value,
      password: this.caregiverPassword.value,
      birthDate: this.caregiverBirthDate.value,
      gender: this.caregiverGender.value,
      address: this.caregiverAddress.value,
      patients: this.associatedPatients
    };

    this.caregiverName.reset(""),
      this.caregiverPassword.reset(""),
      this.caregiverGender.reset(""),
      this.caregiverBirthDate.reset(""),
      this.caregiverAddress.reset(""),

      this.associatedPatients = [];

    this.caregiversService.insertCaregiver(this.insertedCaregiver).subscribe(data => (this.insertedCaregiver = data, this.getAllCaregivers()));
  }

  updateCaregiver() {
    this.updatedCaregiver = {
      id: this.selectedCaregiver.id,
      name: this.caregiverName.value,
      password: this.caregiverPassword.value,
      birthDate: this.caregiverBirthDate.value,
      gender: this.caregiverGender.value,
      address: this.caregiverAddress.value,
      patients: this.associatedPatients
    };

    this.caregiverName.reset(""),
      this.caregiverPassword.reset(""),
      this.caregiverGender.reset(""),
      this.caregiverBirthDate.reset(""),
      this.caregiverAddress.reset(""),

      this.associatedPatients = [];

    this.caregiversService.updateCaregiver(this.updatedCaregiver).subscribe(data => (this.updatedCaregiver = data,this.getAllCaregivers()));
    
  }

  deleteCaregiver() {
    this.caregiverName.reset(""),
      this.caregiverPassword.reset(""),
      this.caregiverGender.reset(""),
      this.caregiverBirthDate.reset(""),
      this.caregiverAddress.reset(""),

      this.associatedPatients = [];
    this.caregiversService.deleteCaregiver(this.foundCaregiver.id).subscribe(data => (this.deletedCaregiver = data, this.getAllCaregivers()));
    
  }

}
