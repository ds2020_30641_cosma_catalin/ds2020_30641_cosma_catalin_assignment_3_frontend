import { Medication } from './medication';

export interface Plan {
    id : string,
    name : string,
    intervals: string,
    duration: string,
    medications : Medication[]
}
