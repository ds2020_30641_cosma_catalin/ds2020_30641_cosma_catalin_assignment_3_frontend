import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MedicationsService } from "../medications.service";
import { Medication } from '../medication';
import { tap } from "rxjs/operators";

@Component({
  selector: 'app-medication-administration',
  templateUrl: './medication-administration.component.html',
  styleUrls: ['./medication-administration.component.css']
})
export class MedicationAdministrationComponent implements OnInit {


  medications = [];

  medicationId = new FormControl("");
  medicationName = new FormControl("");
  medicationSideEffects = new FormControl("");
  medicationDosage = new FormControl("");

  foundMedication : Medication;
  insertedMedication: Medication;
  updatedMedication : Medication;
  selectedMedication: Medication;
  deletedMedication: Medication;

  constructor(private medicationsService: MedicationsService) { }

  ngOnInit(): void {
    this.medicationsService.getMedications().subscribe(data => (this.medications = data));
  }

  RowSelected(u: any) {
    this.selectedMedication = u;
    this.getMedicationById(this.selectedMedication.id).subscribe(
      () => (
        this.medicationName.reset(""),
        this.medicationSideEffects.reset(""),
        this.medicationDosage.reset(""),
        this.medicationName.setValue(this.foundMedication.name),
        this.medicationSideEffects.setValue(this.foundMedication.sideEffects),
        this.medicationDosage.setValue(this.foundMedication.dosage)
      )
    );
  }

  getAllMedications() {
    this.medicationsService
      .getMedications()
      .subscribe(data => (this.medications = data));
  }

  getMedicationById(id: string) {
    return this.medicationsService
      .getMedicationById(id)
      .pipe(tap(data => (this.foundMedication = data)));
  }

  insertNewmedication() {
    this.insertedMedication = {
      id: this.medicationId.value,
      name: this.medicationName.value,
      sideEffects: this.medicationSideEffects.value,
      dosage: this.medicationDosage.value,
    };

    this.medicationName.reset(""),
    this.medicationSideEffects.reset(""),
    this.medicationDosage.reset(""),

    this.medicationsService.insertMedication(this.insertedMedication).subscribe(data => (this.insertedMedication = data, this.getAllMedications()));
  }

  updateMedication(){
    this.updatedMedication = {
      id: this.selectedMedication.id,
      name: this.medicationName.value,
      sideEffects: this.medicationSideEffects.value,
      dosage: this.medicationDosage.value
    };

    this.medicationName.reset(""),
    this.medicationSideEffects.reset(""),
    this.medicationDosage.reset(""),

    this.medicationsService.updateMedication(this.updatedMedication).subscribe(data => (this.updatedMedication = data, this.getAllMedications()));
  }

  deleteMedication(){
    this.medicationName.reset(""),
    this.medicationSideEffects.reset(""),
    this.medicationDosage.reset(""),
    this.medicationsService.deleteMedication(this.foundMedication.id).subscribe(data => (this.deletedMedication = data, this.getAllMedications()));
  }

}
