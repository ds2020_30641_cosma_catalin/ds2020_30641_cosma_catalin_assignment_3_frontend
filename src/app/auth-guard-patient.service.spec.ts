import { TestBed } from '@angular/core/testing';

import { AuthGuardPatientService } from './auth-guard-patient.service';

describe('AuthGuardPatientService', () => {
  let service: AuthGuardPatientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthGuardPatientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
