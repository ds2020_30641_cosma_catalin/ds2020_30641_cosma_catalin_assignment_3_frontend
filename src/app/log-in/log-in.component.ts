import { Component, OnInit } from '@angular/core';
import { FormControl } from "@angular/forms";
import { AuthGuardCaregiverService } from '../auth-guard-caregiver.service';
import { AuthGuardPatientService } from '../auth-guard-patient.service';
import { AuthGuardService } from "../auth-guard.service";
import { Router } from '@angular/router';
import { CaregiversService } from '../caregivers.service';
import { PatientsService } from '../patients.service';
import { LogInService } from '../log-in.service';


@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  uName = new FormControl("");
  uPass = new FormControl("");

  opt = 0;

  caregivers = [];
  patients = [];


  constructor(
    private authGuardService: AuthGuardService,
    private authGuardCaregiverService: AuthGuardCaregiverService,
    private authGuardPatientService: AuthGuardPatientService,
    private router: Router,
    private caregiversService: CaregiversService,
    private patientsService: PatientsService,
    private logInService: LogInService
  ) { }

  ngOnInit(): void {
    this.caregiversService.getCaregivers().subscribe(data => (this.caregivers = data));
    this.patientsService.getPatients().subscribe(data => (this.patients = data));
  }

  checkUserInput() {
    if (this.uName.value == "doctor" && this.uPass.value == "pass123") {
      this.authGuardService.activate = true;
      this.router.navigate(['doctorpage']);
    } else {

      for (let i of this.caregivers) {
        if (this.uName.value == i.name && this.uPass.value == i.password) {
          this.logInService.setCaregiver(i);
          this.authGuardCaregiverService.activate = true;
          this.router.navigate(['caregiverpage']);
        }
      }

      for (let i of this.patients) {
        if (this.uName.value == i.name && this.uPass.value == i.password) {
          this.logInService.setPatient(i);
          this.authGuardPatientService.activate = true;
          this.router.navigate(['patientpage']);
        }
      }
    }
  }

}