import { TestBed } from '@angular/core/testing';

import { AuthGuardCaregiverService } from './auth-guard-caregiver.service';

describe('AuthGuardCaregiverService', () => {
  let service: AuthGuardCaregiverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthGuardCaregiverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
