import { Plan } from './plan';

export interface Patient {
    id: string,
    password: string,
    name: string,
    birthDate: string,
    gender: string,
    address: string,
    medicalRecord: string,
    medicationPlan: Plan
}
