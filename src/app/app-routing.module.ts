import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardCaregiverService } from './auth-guard-caregiver.service';
import { AuthGuardPatientService } from './auth-guard-patient.service';
import { AuthGuardService } from './auth-guard.service';
import { CaregiverPanelComponent } from './caregiver-panel/caregiver-panel.component';
import { DoctorPanelComponent } from './doctor-panel/doctor-panel.component';
import { LogInComponent } from './log-in/log-in.component';
import { PatientPanelComponent } from './patient-panel/patient-panel.component';

const routes: Routes = [
  { path: "", component: LogInComponent },
  { path: "caregiverpage", component: CaregiverPanelComponent, canActivate: [AuthGuardCaregiverService]},
  { path: "patientpage", component: PatientPanelComponent, canActivate: [AuthGuardPatientService] },
  { path: "doctorpage", component: DoctorPanelComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
