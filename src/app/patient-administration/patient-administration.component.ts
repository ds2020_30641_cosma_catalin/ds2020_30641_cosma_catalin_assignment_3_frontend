import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { PatientsService } from "../patients.service";
import { Patient } from '../patient';
import { PlansService } from "../plans.service";
import { Plan } from '../plan';
import { isEmpty, tap } from "rxjs/operators";

@Component({
  selector: 'app-patient-administration',
  templateUrl: './patient-administration.component.html',
  styleUrls: ['./patient-administration.component.css']
})
export class PatientAdministrationComponent implements OnInit {

  patients = [];
  plans = [];
  associatedPlan = {} as Plan;

  patientId = new FormControl("");
  patientName = new FormControl("");
  patientPassword = new FormControl("");
  patientBirthDate = new FormControl("");
  patientGender = new FormControl("");
  patientAddress = new FormControl("");
  patientMedicalRecord = new FormControl("");

  foundPatient: Patient;
  insertedPatient: Patient;
  updatedPatient: Patient;
  selectedPatient: Patient;
  deletedPatient: Patient;

  foundPlan= {} as Plan;
  selectedPlan= {} as Plan;

  planToAdd = {} as Plan ;
  planToRemove = {} as Plan ;
  planUpdated = {} as Plan;
  plansAvailable = {} as Plan;

  constructor(private patientsService: PatientsService, private plansService: PlansService) { }

  ngOnInit(): void {
    this.patientsService.getPatients().subscribe(data => (this.patients = data));
    this.plansService.getPlans().subscribe(data => (this.plans = data));
  }

  rowSelected(u: any) {
    this.selectedPatient = u;
    this.getPatientById(this.selectedPatient.id).subscribe(
      () => (
        this.patientName.reset(""),
        this.patientPassword.reset(""),
        this.patientGender.reset(""),
        this.patientBirthDate.reset(""),
        this.patientAddress.reset(""),
        this.patientMedicalRecord.reset(""),
        this.patientName.setValue(this.foundPatient.name),
        this.patientPassword.setValue(this.foundPatient.password),
        this.patientGender.setValue(this.foundPatient.gender),
        this.patientBirthDate.setValue(this.foundPatient.birthDate),
        this.patientAddress.setValue(this.foundPatient.address),
        this.patientMedicalRecord.setValue(this.foundPatient.medicalRecord),

        this.associatedPlan = this.foundPatient.medicationPlan
      )
    );
  }

  addPlanToAssociatedList() {
      this.associatedPlan = this.foundPlan;
  }

  planRowSelected(u: any) {
    this.selectedPlan = u;
    this.getPlanById(this.selectedPlan.id).subscribe(
      () => (
        this.addPlanToAssociatedList()
      )
    );
  }

  associatedRowSelected() {
    this.associatedPlan = null;
  }

  getAllPatients() {
    this.patientsService
      .getPatients()
      .subscribe(data => (this.patients = data));
  }

  getAllPlans() {
    this.plansService
      .getPlans()
      .subscribe(data => (this.plans = data));
  }

  getPatientById(id: string) {
    return this.patientsService
      .getPatientById(id)
      .pipe(tap(data => (this.foundPatient = data)));
  }

  getPlanById(id: string) {
    return this.plansService
      .getPlanById(id)
      .pipe(tap(data => (this.foundPlan = data)));
  }

  isEmptyObject(obj) {
    return !Object.keys(obj).length;
}

  insertNewPatient() {
    if(this.associatedPlan == null){
      this.associatedPlan = null;
    }else{
      if(this.isEmptyObject(this.associatedPlan)){
        this.associatedPlan = null;
      }
    }
    this.insertedPatient = {
      id: this.patientId.value,
      name: this.patientName.value,
      password: this.patientPassword.value,
      birthDate: this.patientBirthDate.value,
      gender: this.patientGender.value,
      address: this.patientAddress.value,
      medicalRecord: this.patientMedicalRecord.value,
      medicationPlan: this.associatedPlan
    };
    this.patientName.reset("");
    this.patientPassword.reset("");
    this.patientGender.reset("");
    this.patientBirthDate.reset("");
    this.patientAddress.reset("");
    this.patientMedicalRecord.reset("");

    this.associatedPlan = {} as Plan;

    this.patientsService.insertPatient(this.insertedPatient).subscribe(data => (this.insertedPatient = data, this.getAllPatients()));
  }

  updatePatient() {
    this.updatedPatient = {
      id: this.selectedPatient.id,
      name: this.patientName.value,
      password: this.patientPassword.value,
      birthDate: this.patientBirthDate.value,
      gender: this.patientGender.value,
      address: this.patientAddress.value,
      medicalRecord: this.patientMedicalRecord.value,
      medicationPlan: this.associatedPlan
    };

    this.patientName.reset("");
    this.patientPassword.reset("");
    this.patientGender.reset("");
    this.patientBirthDate.reset("");
    this.patientAddress.reset("");
    this.patientMedicalRecord.reset("");

    this.associatedPlan = {} as Plan;

    this.patientsService.updatePatient(this.updatedPatient).subscribe(data => (this.updatedPatient = data,this.getAllPatients()));

    
    
  }

  deletePatient() {
    this.patientName.reset("");
    this.patientPassword.reset("");
    this.patientGender.reset("");
    this.patientBirthDate.reset("");
    this.patientAddress.reset("");
    this.patientMedicalRecord.reset("");

    this.associatedPlan = {} as Plan;
    this.patientsService.deletePatient(this.foundPatient.id).subscribe(data => (this.deletedPatient = data, this.getAllPatients()));
    
  }

}
