import { Component, OnInit } from '@angular/core';
import { CaregiversService } from "../caregivers.service";
import { Caregiver } from '../caregiver';
import { PatientsService } from "../patients.service";
import { Patient } from '../patient';
import { tap } from "rxjs/operators";
import { LogInService } from '../log-in.service';
import { PlansService } from "../plans.service";
import { Plan } from '../plan';
import { FormControl } from '@angular/forms';

declare var SockJS;
declare var Stomp;


@Component({
  selector: 'app-caregiver-panel',
  templateUrl: './caregiver-panel.component.html',
  styleUrls: ['./caregiver-panel.component.css']
})
export class CaregiverPanelComponent implements OnInit {

  patients = [];
  plans = [];

  foundCaregiver: Caregiver;
  selectedPatient: Patient;

  associatedPatients = [];
  associatedPlans = [];
  associatedMedications = [];

  foundPatient: Patient;
  foundPlan: Plan;
  selectedPlan: Plan;

  stompClient;

  messageForm = new FormControl("");

  constructor(private logInService: LogInService,
    private patientsService: PatientsService,
    private plansService: PlansService) { }

  ngOnInit(): void {
    this.foundCaregiver = this.logInService.getCaregiver();
    this.associatedPatients = this.foundCaregiver.patients;
    this.initializeWebSocketConnection();
  }
  

  initializeWebSocketConnection() {
    const serverUrl = 'https://om3-platform.herokuapp.com/socket';
    const ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/message', (message) => {
        if (message.body) {
          that.messageForm.setValue(message.body);
        }
      });
    });
  }

  associatedRowSelected(u: any) {
    this.selectedPatient = u;
    this.getPatientById(this.selectedPatient.id).subscribe(
      () => (
        this.associatedPlans = [],
        this.associatedPlans.push(this.foundPatient.medicationPlan),
        this.associatedMedications = [],
        this.associatedMedications = this.selectedPatient.medicationPlan.medications

      )
    );
  }


  getPatientById(id: string) {
    return this.patientsService
      .getPatientById(id)
      .pipe(tap(data => (this.foundPatient = data)));
  }

  getPlanById(id: string) {
    return this.plansService
      .getPlanById(id)
      .pipe(tap(data => (this.foundPlan = data)));
  }

}
