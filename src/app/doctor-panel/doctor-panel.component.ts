import { Component, OnInit } from "@angular/core";

@Component({
  selector: 'app-doctor-panel',
  templateUrl: './doctor-panel.component.html',
  styleUrls: ['./doctor-panel.component.css']
})
export class DoctorPanelComponent implements OnInit {


  displayCase = 0;

  constructor() { }

  ngOnInit(): void {
  }

  setDisplayCase(c: number) {
    this.displayCase = c;
  }

}
