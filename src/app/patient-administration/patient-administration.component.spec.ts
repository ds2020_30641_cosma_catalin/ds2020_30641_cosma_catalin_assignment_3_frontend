import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientAdministrationComponent } from './patient-administration.component';

describe('PatientAdministrationComponent', () => {
  let component: PatientAdministrationComponent;
  let fixture: ComponentFixture<PatientAdministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientAdministrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
