import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Patient } from "./patient";

@Injectable({
  providedIn: 'root'
})
export class PatientsService {

  private getAllPatientsUrl = "https://om3-platform.herokuapp.com/patient/all";
  private getPatientByIdUrl = "https://om3-platform.herokuapp.com/patient/get";
  private insertPatientUrl = "https://om3-platform.herokuapp.com/patient/insert";
  private updatePatientUrl = "https://om3-platform.herokuapp.com/patient/update";
  private deletePatientUrl = "https://om3-platform.herokuapp.com/patient/delete";

  constructor(private http: HttpClient) { }

  getPatients(): Observable<Patient[]> {
    return this.http.get<Patient[]>(this.getAllPatientsUrl);
  }

  getPatientById(id: string): Observable<Patient> {
    return this.http.get<Patient>(this.getPatientByIdUrl + id);
  }

  insertPatient(patient: Patient): Observable<Patient> {
    return this.http.post<Patient>(this.insertPatientUrl, patient);
  }

  updatePatient(patient: Patient): Observable<Patient> {
    return this.http.put<Patient>(this.updatePatientUrl, patient);
  }

  deletePatient(id: string): Observable<Patient> {
    return this.http.delete<Patient>(this.deletePatientUrl + id);
  }
}
