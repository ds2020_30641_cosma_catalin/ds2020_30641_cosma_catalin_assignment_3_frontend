import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Medication } from "./medication";

@Injectable({
  providedIn: 'root'
})
export class MedicationsService {

  private getAllMedicationsUrl = "https://om3-platform.herokuapp.com/medication/all";
  private getMedicationByIdUrl = "https://om3-platform.herokuapp.com/medication/get";
  private insertMedicationUrl = "https://om3-platform.herokuapp.com/medication/insert";
  private updateMedicationUrl = "https://om3-platform.herokuapp.com/medication/update";
  private deleteMedicationUrl = "https://om3-platform.herokuapp.com/medication/delete";

  constructor(private http: HttpClient) { }

  getMedications(): Observable<Medication[]> {
    return this.http.get<Medication[]>(this.getAllMedicationsUrl);
  }

  getMedicationById(id: string): Observable<Medication> {
    return this.http.get<Medication>(this.getMedicationByIdUrl + id);
  }

  insertMedication(Medication: Medication): Observable<Medication> {
    return this.http.post<Medication>(this.insertMedicationUrl, Medication);
  }

  updateMedication(Medication: Medication): Observable<Medication> {
    return this.http.put<Medication>(this.updateMedicationUrl, Medication);
  }

  deleteMedication(id: string): Observable<Medication> {
    return this.http.delete<Medication>(this.deleteMedicationUrl + id);
  }
}
