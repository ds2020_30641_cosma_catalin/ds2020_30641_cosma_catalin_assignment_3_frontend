import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MedicationsService } from "../medications.service";
import { Medication } from '../medication';
import { PlansService } from "../plans.service";
import { Plan } from '../plan';
import { tap } from "rxjs/operators";

@Component({
  selector: 'app-plan-administration',
  templateUrl: './plan-administration.component.html',
  styleUrls: ['./plan-administration.component.css']
})
export class PlanAdministrationComponent implements OnInit {

  plans = [];
  medications = [];
  associatedMedications = [];
  names = [];

  planId = new FormControl("");
  planName = new FormControl("");
  planIntervals = new FormControl("");
  planDuration = new FormControl("");

  foundPlan: Plan;
  insertedPlan: Plan;
  updatedPlan: Plan;
  selectedPlan: Plan;
  deletedPlan: Plan;

  foundMedication: Medication;
  selectedMedication: Medication;

  medicationsToAdd = [];
  medicationsToRemove = [];
  medicationsUpdated = [];
  medicationsAvailable = [];

  constructor(private plansService: PlansService, private medicationsService: MedicationsService) { }

  ngOnInit(): void {
    this.plansService.getPlans().subscribe(data => (this.plans = data));
    this.medicationsService.getMedications().subscribe(data => (this.medications = data));
  }

  rowSelected(u: any) {
    this.selectedPlan = u;
    this.getPlanById(this.selectedPlan.id).subscribe(
      () => (
        this.planName.reset(""),
        this.planIntervals.reset(""),
        this.planDuration.reset(""),
        this.planName.setValue(this.foundPlan.name),
        this.planIntervals.setValue(this.foundPlan.intervals),
        this.planDuration.setValue(this.foundPlan.duration),
        this.associatedMedications = this.foundPlan.medications,
        this.medicationsUpdated = this.foundPlan.medications
      )
    );
  }

  addMedicationToAssociatedList() {
    for (let i of this.associatedMedications) {
      this.names.push(i.name);
    }

    if (this.names.includes(this.foundMedication.name) == false) {
      this.associatedMedications.push(this.foundMedication);
    }

    this.names = [];

  }

  medicationRowSelected(u: any) {
    this.selectedMedication = u;
    this.getMedicationById(this.selectedMedication.id).subscribe(
      () => (
        this.addMedicationToAssociatedList()
      )
    );
  }

  associatedRowSelected(u: any) {
    this.selectedMedication = u;
    this.getMedicationById(this.selectedMedication.id).subscribe(
      () => (this.associatedMedications.splice(this.associatedMedications.indexOf(this.selectedMedication), 1)
      )
    );
  }

  getAllPlans() {
    this.plansService
      .getPlans()
      .subscribe(data => (this.plans = data));
  }

  getAllMedications() {
    this.medicationsService
      .getMedications()
      .subscribe(data => (this.medications = data));
  }

  getPlanById(id: string) {
    return this.plansService
      .getPlanById(id)
      .pipe(tap(data => (this.foundPlan = data)));
  }

  getMedicationById(id: string) {
    return this.medicationsService
      .getMedicationById(id)
      .pipe(tap(data => (this.foundMedication = data)));
  }

  insertNewPlan() {
    this.insertedPlan = {
      id: this.planId.value,
      name: this.planName.value,
      intervals: this.planIntervals.value,
      duration: this.planDuration.value,
      medications: this.associatedMedications
    };

    this.planName.reset(""),
      this.planIntervals.reset(""),
      this.planDuration.reset(""),

      this.associatedMedications = [];

    this.plansService.insertPlan(this.insertedPlan).subscribe(data => (this.insertedPlan = data, this.getAllPlans()));
  }

  updatePlan() {
    this.updatedPlan = {
      id: this.selectedPlan.id,
      name: this.planName.value,
      intervals: this.planIntervals.value,
      duration: this.planDuration.value,
      medications: this.associatedMedications
    };

    this.planName.reset(""),
      this.planIntervals.reset(""),
      this.planDuration.reset(""),

      this.associatedMedications = [];

    this.plansService.updatePlan(this.updatedPlan).subscribe(data => (this.updatedPlan = data, this.getAllPlans()));

  }

  deletePlan() {

    this.planName.reset(""),
      this.planIntervals.reset(""),
      this.planDuration.reset(""),

      this.associatedMedications = [];

    this.plansService.deletePlan(this.foundPlan.id).subscribe(data => (this.deletedPlan = data, this.getAllPlans()));

  }

}