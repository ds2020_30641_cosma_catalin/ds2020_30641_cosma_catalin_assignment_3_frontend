import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Caregiver } from "./caregiver";

@Injectable({
  providedIn: 'root'
})
export class CaregiversService {

  private getAllCaregiversUrl = "https://om3-platform.herokuapp.com/caregiver/all";
  private getCaregiverByIdUrl = "https://om3-platform.herokuapp.com/caregiver/get";
  private insertCaregiverUrl = "https://om3-platform.herokuapp.com/caregiver/insert";
  private updateCaregiverUrl = "https://om3-platform.herokuapp.com/caregiver/update";
  private deleteCaregiverUrl = "https://om3-platform.herokuapp.com/caregiver/delete";

  constructor(private http: HttpClient) { }

  getCaregivers(): Observable<Caregiver[]> {
    return this.http.get<Caregiver[]>(this.getAllCaregiversUrl);
  }

  getCaregiverById(id: string): Observable<Caregiver> {
    return this.http.get<Caregiver>(this.getCaregiverByIdUrl + id);
  }

  insertCaregiver(caregiver: Caregiver): Observable<Caregiver> {
    return this.http.post<Caregiver>(this.insertCaregiverUrl, caregiver);
  }

  updateCaregiver(caregiver: Caregiver): Observable<Caregiver> {
    return this.http.put<Caregiver>(this.updateCaregiverUrl, caregiver);
  }

  deleteCaregiver(id: string): Observable<Caregiver> {
    return this.http.delete<Caregiver>(this.deleteCaregiverUrl + id);
  }
}
