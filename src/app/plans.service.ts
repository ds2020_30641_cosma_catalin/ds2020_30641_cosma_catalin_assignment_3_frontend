import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Plan } from "./plan";

@Injectable({
  providedIn: 'root'
})
export class PlansService {

  private getAllPlansUrl = "https://om3-platform.herokuapp.com/plan/all";
  private getPlanByIdUrl = "https://om3-platform.herokuapp.com/plan/get";
  private insertPlanUrl = "https://om3-platform.herokuapp.com/plan/insert";
  private updatePlanUrl = "https://om3-platform.herokuapp.com/plan/update";
  private deletePlanUrl = "https://om3-platform.herokuapp.com/plan/delete";

  constructor(private http: HttpClient) { }

  getPlans(): Observable<Plan[]> {
    return this.http.get<Plan[]>(this.getAllPlansUrl);
  }

  getPlanById(id: string): Observable<Plan> {
    return this.http.get<Plan>(this.getPlanByIdUrl + id);
  }

  insertPlan(Plan: Plan): Observable<Plan> {
    return this.http.post<Plan>(this.insertPlanUrl, Plan);
  }

  updatePlan(Plan: Plan): Observable<Plan> {
    return this.http.put<Plan>(this.updatePlanUrl, Plan);
  }

  deletePlan(id: string): Observable<Plan> {
    return this.http.delete<Plan>(this.deletePlanUrl + id);
  }
}
