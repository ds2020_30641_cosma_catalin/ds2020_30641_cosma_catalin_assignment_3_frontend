import { Injectable } from '@angular/core';
import { Caregiver } from './caregiver';
import { Patient } from './patient';

@Injectable({
  providedIn: 'root'
})
export class LogInService {


  foundCaregiver: Caregiver;
  foundPatient: Patient;

  constructor() { }

  setCaregiver(c: Caregiver){
    this.foundCaregiver = c;
  }

  getCaregiver(): Caregiver{
    return this.foundCaregiver;
  }

  setPatient(p: Patient){
    this.foundPatient = p;
  }

  getPatient(): Patient{
    return this.foundPatient;
  }



}
