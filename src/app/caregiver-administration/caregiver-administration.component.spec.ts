import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregiverAdministrationComponent } from './caregiver-administration.component';

describe('CaregiverAdministrationComponent', () => {
  let component: CaregiverAdministrationComponent;
  let fixture: ComponentFixture<CaregiverAdministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaregiverAdministrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregiverAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
