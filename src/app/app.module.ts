import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { LogInComponent } from './log-in/log-in.component';
import { DoctorPanelComponent } from './doctor-panel/doctor-panel.component';
import { CaregiverPanelComponent } from './caregiver-panel/caregiver-panel.component';
import { PatientPanelComponent } from './patient-panel/patient-panel.component';

import { AuthGuardService } from './auth-guard.service';
import { PatientsService } from './patients.service';
import { MedicationsService } from './medications.service';
import { CaregiversService } from './caregivers.service';
import { PlansService } from "./plans.service";
import { PatientAdministrationComponent } from './patient-administration/patient-administration.component';
import { CaregiverAdministrationComponent } from './caregiver-administration/caregiver-administration.component';
import { MedicationAdministrationComponent } from './medication-administration/medication-administration.component';
import { PlanAdministrationComponent } from './plan-administration/plan-administration.component';
import { AuthGuardCaregiverService } from './auth-guard-caregiver.service';
import { AuthGuardPatientService } from './auth-guard-patient.service';
import { LogInService } from './log-in.service';


@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    LogInComponent,
    DoctorPanelComponent,
    CaregiverPanelComponent,
    PatientPanelComponent,
    PatientAdministrationComponent,
    CaregiverAdministrationComponent,
    MedicationAdministrationComponent,
    PlanAdministrationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [AuthGuardService, AuthGuardCaregiverService, AuthGuardPatientService ,PatientsService, MedicationsService, CaregiversService, PlansService, LogInService],
  bootstrap: [AppComponent]
})
export class AppModule { }
