import { from } from 'rxjs';
import {Patient} from "./patient";

export interface Caregiver {
    id: string,
    name: string,
    password: string,
    birthDate: string,
    gender: string,
    address: string,
    patients: Patient[]
}
