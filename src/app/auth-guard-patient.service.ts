import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardPatientService implements CanActivate {

  activate = false;
  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if (this.activate == true) {
      return true;
    } else {
      alert('Username or password is incorect !')
      this.router.navigate(['']);
      return false;
    }
  }


}