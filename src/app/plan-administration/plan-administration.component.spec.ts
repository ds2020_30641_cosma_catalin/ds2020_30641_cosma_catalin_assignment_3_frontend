import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanAdministrationComponent } from './plan-administration.component';

describe('PlanAdministrationComponent', () => {
  let component: PlanAdministrationComponent;
  let fixture: ComponentFixture<PlanAdministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanAdministrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
