import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregiverPanelComponent } from './caregiver-panel.component';

describe('CaregiverPanelComponent', () => {
  let component: CaregiverPanelComponent;
  let fixture: ComponentFixture<CaregiverPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaregiverPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregiverPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
