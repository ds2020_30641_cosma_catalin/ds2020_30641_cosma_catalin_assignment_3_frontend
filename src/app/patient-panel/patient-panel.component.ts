import { Component, OnInit } from '@angular/core';
import { LogInService } from '../log-in.service';
import { Patient } from '../patient';
import { Plan } from '../plan';
import { HttpClient } from "@angular/common/http";
import { Medication } from '../medication';

@Component({
  selector: 'app-patient-panel',
  templateUrl: './patient-panel.component.html',
  styleUrls: ['./patient-panel.component.css']
})
export class PatientPanelComponent implements OnInit {

  foundPatient: Patient;
  foundPlan = {} as Plan;

  planToDisplay = {} as Plan;

  time = new Date();
  timer;

  notTaken = false;

  constructor(private logInService: LogInService, private http: HttpClient) { }

  ngOnInit(): void {
    this.foundPatient = this.logInService.getPatient();
    this.downloadMedicationPlan();

    this.timer = setInterval(() => {
      this.time = new Date();

      let hours = this.time.getHours();
      let minutes = this.time.getMinutes();
      let seconds = this.time.getSeconds();

      let ih1 = Number(this.foundPlan.intervals.slice(0, 2));
      let im1 = Number(this.foundPlan.intervals.slice(3, 5));

      let ih2 = Number(this.foundPlan.intervals.slice(8, 10));
      let im2 = Number(this.foundPlan.intervals.slice(11, 13));

      if (hours == 13 && minutes == 35 && seconds == 0) {
        this.downloadMedicationPlan();
      }

      if ((hours > ih1 && hours < ih2) || (hours == ih1 && minutes >= im1 && ih1 != ih2) || (hours == ih2 && minutes < im2 && ih1 != ih2)
        || (hours == ih1 && minutes >= im1 && minutes <= im2 && ih1 == ih2)
      ) {
        this.planToDisplay = this.foundPlan;
      }

      if (hours > ih2 || (hours == ih2 && minutes >= im2 && this.notTaken == false)) {
        this.notTaken = true;
        console.log("Medications not taken !");
        this.alertMedicationNotTaken();
      }

    }, 1000);
  }

  alertMedicationNotTaken() {
    let meds: string = "";

    for (var s of this.planToDisplay.medications) {
      meds = s.name + ", " + meds;
    }
    meds = meds.slice(0, -2);
    let stime: string = String(this.time.getHours()) + ":" + String(this.time.getMinutes()) + ":" + String(this.time.getSeconds());
    this.displayMedicationStatus(this.foundPatient.name, meds, "not taken", stime);

    this.planToDisplay.medications = [];
  }

  setMedicationStatus(med: Medication) {
    const index: number = this.planToDisplay.medications.indexOf(med);
    if (index !== -1) {
      this.planToDisplay.medications.splice(index, 1);
    }
    let stime: string = String(this.time.getHours()) + ":" + String(this.time.getMinutes()) + ":" + String(this.time.getSeconds());
    this.displayMedicationStatus(this.foundPatient.name, med.name, "taken", stime);
  }


  displayMedicationStatus(patientName: String, medicationName: String, medicationStatus: String, statusTime: String) {
    const url = "https://om3-platform.herokuapp.com/rpc/myservice";

    this.http
      .post(url, {
        id: 0,
        method: "displayMedicationStatus",
        params: [
          patientName,
          medicationName,
          medicationStatus,
          statusTime
        ]
      })
      .subscribe();
  }


  downloadMedicationPlan() {
    const url = "https://om3-platform.herokuapp.com/rpc/myservice";

    this.http
      .post(url, {
        id: 0,
        method: "downloadMedicationPlan",
        params: [
          this.foundPatient.name
        ]
      })
      .subscribe(response => {
        this.foundPlan = response['result']['body']
      });
  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }

}

